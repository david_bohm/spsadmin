import React from 'react';

import TextInput from '@material-ui/core/TextField';

const CustomTextInput = ({record}) => (
    <span>
        {record.birthdate.substring(0, 10)}
    </span>
)

CustomTextInput.defaultProps = { addLabel: true };

export default CustomTextInput;