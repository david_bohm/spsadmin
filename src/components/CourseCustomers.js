import React from 'react';

import axios from 'axios';

import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';

class CourseCustomers extends React.Component {
    constructor(props) {
        super(props);

        this.daysOfWeek = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

    }

    state = {
        course: {
            customers: [],
            courseDates: [],
            days: [
                { name: 'Lunes', short: 'L', info: {id: false} },
                { name: 'Martes', short: 'M' , info: {id: false}},
                { name: 'Miércoles', short: 'X', info: {id: false} },
                { name: 'Jueves', short: 'J', info: {id: false} },
                { name: 'Viernes', short: 'V' , info: {id: false}},
                { name: 'Sábado', short: 'S' , info: {id: false}},
                { name: 'Domingo', short: 'D', info: {id: false} },
            ]
        },
        ready: false
    }

    componentDidMount() {
        fetch(`http://157.230.58.139:3000/api/Courses/${this.props.record.id}/Customers`,
            {
                method: 'GET',
                headers: {
                    'Authorization': JSON.parse(localStorage.getItem('lbtoken')).value.id
                }
            })
            .then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(customers => {
                // console.log('Customers :', customers);
                const courseDatefilter = `?filter={"where":{"id_course": "${this.props.record.id}"}}`;

                this.setState({
                    ...this.state,
                    course: {
                        ...this.state.course,
                        customers: customers
                    }
                });
                fetch(`http://157.230.58.139:3000/api/CourseDates${courseDatefilter}`,
                    {
                        method: 'GET',
                        headers: {
                            'Authorization': JSON.parse(localStorage.getItem('lbtoken')).value.id
                        }
                    })
                    .then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(courseDates => {
                        let daysCopy = [...this.state.course.days];

                        this.state.course.days.forEach((day, index) => {
                            courseDates.forEach(courseDate => {
                                if (day.short === courseDate.day) {
                                    daysCopy[index].merca = true;
                                    daysCopy[index].info  = courseDate;
                                    this.setState({
                                        ...this.state,
                                        course: {
                                            ...this.state.course,
                                            days: [
                                                ...daysCopy
                                            ]
                                        }
                                    }, () => {
                                        // console.log(this.state.course.days);
                                    })
                                }
                            });
                        })

                        this.setState({
                            ...this.state,
                            course: {
                                ...this.state.course,
                                courseDates: courseDates
                            }
                        });

                        let promises = [];
                        let parsedResponse = [];

                        customers.forEach(c => {
                            const customerCoursesFilter = `?filter={"where":{"id_course": "${this.props.record.id}", "id_customer": "${c.id}"}}`;

                            promises.push(
                                fetch(`http://157.230.58.139:3000/api/CustomerCourses${customerCoursesFilter}`,
                                    {
                                        method: 'GET',
                                        headers: {
                                            'Authorization': JSON.parse(localStorage.getItem('lbtoken')).value.id
                                        }
                                    })
                            )

                            let i = 0;

                            Promise.all(promises)
                                .then(
                                    value => {

                                        if (value.length === promises.length) {
                                            value.forEach(res => {
                                                res.json().then(v => {
                                                    parsedResponse.push(v);
                                                    // console.log(parsedResponse.length, promises.length);

                                                    if (parsedResponse.length === promises.length) {
                                                        parsedResponse.forEach(customerCourse => {
                                                            let currentCustomerIndex;
                                                            let arrayCopy;

                                                            // console.log(customerCourse[0].id_customer);

                                                            const matchedCustomer = this.state.course.customers.filter(
                                                                (c, i) => {
                                                                    if (c.id === customerCourse[0].id_customer) {
                                                                        // console.log('Matched :', customerCourse[0].id_customer, c.name)
                                                                        currentCustomerIndex = i;
                                                                        arrayCopy = [...this.state.course.customers];
                                                                    }

                                                                    return c.id === customerCourse[0].id_customer;
                                                                }
                                                            )

                                                            // console.log(matchedCustomer.length, matchedCustomer[0].name);

                                                            matchedCustomer[0].course = customerCourse[0];
                                                            arrayCopy[currentCustomerIndex] = matchedCustomer[0];

                                                            // console.log('Last', arrayCopy);

                                                            this.setState({
                                                                ...this.state,
                                                                course: {
                                                                    ...this.state.course,
                                                                    customers: [
                                                                        ...arrayCopy
                                                                    ]
                                                                }
                                                            }, () => {
                                                                i++;
                                                                if (i === this.state.course.customers.length) {
                                                                    // console.log(i);
                                                                    // console.log('State updated');

                                                                    this.state.course.customers.forEach((c, idx) => {
                                                                        const customerCourseDatesFilter = `?filter={"where":{"id_customercourse": "${c.course.id}"}}`;

                                                                        fetch(`http://157.230.58.139:3000/api/CustomerCourseDates${customerCourseDatesFilter}`,
                                                                            {
                                                                                method: 'GET',
                                                                                headers: {
                                                                                    'Authorization': JSON.parse(localStorage.getItem('lbtoken')).value.id
                                                                                }
                                                                            })
                                                                            .then(res => res.json())
                                                                            .catch(error => console.error('Error:', error))
                                                                            .then(customerCourseDates => {

                                                                                let arrayCopy_2 = [...this.state.course.customers];

                                                                                arrayCopy_2[idx].courseDates = customerCourseDates.filter(cCd => cCd.status === true);

                                                                                this.setState({
                                                                                    ...this.state,
                                                                                    course: {
                                                                                        ...this.state.course,
                                                                                        customers: [
                                                                                            ...arrayCopy_2
                                                                                        ]
                                                                                    }
                                                                                }, () => {
                                                                                    if (idx === this.state.course.customers.length - 1) {
                                                                                        // console.log('READY MF');
                                                                                        this.setState({
                                                                                            ...this.state,
                                                                                            ready: true
                                                                                        }, () => {
                                                                                            this.state.course.customers.forEach(c => {
                                                                                                this.state.course.days.forEach(d => {
                                                                                                    console.log(c.courseDates.length);
                                                                                                })
                                                                                            })
                                                                                        });

                                                                                    }
                                                                                });
                                                                             });
                                                                    })
                                                                }
                                                            })
                                                        })
                                                    }
                                                });
                                            })
                                        }
                                    },
                                    reason => {
                                        // console.log('Error :', reason);
                                    }
                                );
                        });
                    });
            });
    }



    render() {
        return (
            <Paper>
                <Table >
                    <TableHead>
                        <TableRow>
                            <TableCell>Socio</TableCell>
                            {
                                this.daysOfWeek.map((day, idx) => (
                                    <TableCell align="right" key={`courseDayKey-${idx}`}>{day}</TableCell>
                                ))
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {
                                this.state.ready ?
                                    this.state.course.customers.map(c => {
                                        return (
                                        <TableRow key={c.index}>
                                            <TableCell>
                                                {c.name}
                                            </TableCell>
                                          {
                                              this.state.course.days.map(d => {
                                                  console.log('HEY?', c.courseDates);
                                                //   if (c.courseDates !== undefined) {
                                                    if (c.courseDates) {
                                                        if (d.merca) {
                                                            return <TableCell>{
                                                                c.courseDates.map(cDate => {
                                                                    if (cDate.id_coursedate === d.info.id) {
                                                                        return <span>SI</span>
                                                                    } else {
                                                                        return <span></span>
                                                                    }
                                                                })
                                                            }</TableCell>
                                                        } else {
                                                            return (<TableCell></TableCell>)
                                                        }
                                                      } else {
                                                        return (<TableCell></TableCell>)
    
                                                      }
                                                  }
                                              
                                                
                                            )
                                          }
                                        </TableRow>)
                                    }) : null
                            }
                    </TableBody>
                </Table>
            </Paper>
        )
    }

}

export default CourseCustomers;