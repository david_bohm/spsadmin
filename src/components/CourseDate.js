import React from 'react';

import axios from 'axios';

import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';

const API_URL = 'http://157.230.58.139:3000/api';

const styles = {
    table: {
        maxWidth: '80%'
    }
};

class DayDate extends React.Component {
    state = {
        checked: this.props.status
    }

    handleCheck = () => {
        this.setState({
            checked: !this.state.checked
        }, () => {
            this.props.handleChange({
                day: this.props.day,
                value: this.state.checked,
                type: 'status'
            })
        });
    }

    render() {
        return (
            <>
                <TextField
                    label="Hora de inicio"
                    style={{
                        width: 100
                    }}
                    value={this.props.start}
                    onChange={(event) => {
                        this.props.handleChange({
                            day: this.props.day,
                            value: event.target.value,
                            type: 'start'
                        });

                        if (!this.state.checked) this.handleCheck();

                    }} />
                <TextField
                    label="Duración (minutos)"
                    style={{
                        width: 100
                    }}
                    value={this.props.duration}
                    onChange={(event) => {
                        this.props.handleChange({
                            day: this.props.day,
                            value: event.target.value,
                            type: 'duration'
                        });
                    }} />
                <Checkbox checked={this.state.checked} onChange={this.handleCheck} />
            </>
        )
    }
}

class CourseDate extends React.Component {
    constructor(props) {
        super(props);

        this.daysOfWeek = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

        this.courseId = props.courseId ? props.courseId : props.record.id_course;
    }

    state = {
        days: {
            'L': {
                start: '',
                duration: '',
                status: false,
                id: null
            },
            'M': {
                start: '',
                duration: '',
                status: false,
                id: null
            },
            'X': {
                start: '',
                duration: '',
                status: false,
                id: null
            },
            'J': {
                start: '',
                duration: '',
                status: false,
                id: null
            },
            'V': {
                start: '',
                duration: '',
                status: false,
                id: null
            },
            'S': {
                start: '',
                duration: '',
                status: false,
                id: null
            },
            'D': {
                start: '',
                duration: '',
                status: false,
                id: null
            }
        },
        ready: false,
        limit: null
    }

    componentDidMount() {
        console.log(this.props);

        const filter = `filter={"where":{"id_course": "${this.courseId}"}}`;

        fetch(`${API_URL}/CourseDates?${filter}`, 
            {
                method: 'GET',
                headers: {
                    'Authorization': JSON.parse(localStorage.getItem('lbtoken')).value.id
                }
            })
            .then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => {
                this.mergeDays(response);
            });
    }
    
    mergeDays = (courseDates) => {
        courseDates.forEach(courseDate => {
            if (courseDate.quote) {
                this.setState({
                    ...this.state,
                    limit: courseDate.quote
                })
            }

            if (this.state.days[courseDate.day]) {
                this.setState({
                    ...this.state,
                    days: {
                        ...this.state.days,
                        [courseDate.day] : {
                            start    : courseDate.time_from,
                            duration : courseDate.hours,
                            id       : courseDate.id,
                            status   : courseDate.status
                        }
                    }
                })
            }
        });

        this.setState({
            ...this.state,
            ready: true
        });
    }

    handleChange = (config) => {
        console.log(this.state.days[config.day]);

        this.setState({
            ...this.state,
            days: {
                ...this.state.days,
                [config.day]: {
                    ...this.state.days[config.day],
                    [config.type]: config.value
                }
            }
        });
    }
    
    renderTable = () => {
        let courseDates = [];

        Object.entries(this.state.days).forEach(entry => {
            const day   = entry[0];
            const props = entry[1];

            courseDates.push({
                day      : day,
                start    : props.start,
                duration : props.duration,
                status   : props.status
            });
        });

        return courseDates;
    }

    handleUpdate = () => {
        let promises = [];

        Object.entries(this.state.days).forEach(entry => {
            const day   = entry[0];
            const props = entry[1];

            promises.push(this.insertCourseDate(props, day));
        });
        
        Promise.all(promises)
            .then(
                values => {
                    window.alert('Acutalizado correctamente');
            },  reason => {
                    console.log(reason)
            });
    }
    
    insertCourseDate = (courseDate, day) => {
        const postData = {
            id_course : this.courseId, 
            hours     : courseDate.duration,
            time_from : courseDate.start,
            day       : day,
            status    : courseDate.status,
            quote     : this.state.limit
        }

        if (
            (courseDate.start && courseDate.duration) === '' && 
            !courseDate.id) {
                console.log('Empty date', courseDate);
                return
            }

        if (courseDate.id) {
            return axios(`${API_URL}/CourseDates/${courseDate.id}`,
                {
                    method  : 'PUT',
                    data    : {
                        ...postData
                    },
                    headers : {
                        'Authorization': JSON.parse(localStorage.getItem('lbtoken')).value.id
                    }
                })

        } else {
            return axios(`${API_URL}/CourseDates`,
                {
                    method  : 'POST',
                    data    : {
                        ...postData
                    },
                    headers : {
                        'Authorization': JSON.parse(localStorage.getItem('lbtoken')).value.id
                    }
                })
        }
    }

    render() {
        const { classes } = this.props;

        return (
            <Paper>
                <div style={{padding: 20}}>
                <span>Lìmite de socios</span>
                <input label="Lìmite de socios"
                               type="number"
                               style={{marginLeft: 20}}
                               value={this.state.limit}
                               onChange={(event) => {
                                 this.setState({
                                     ...this.state,
                                     limit: event.target.value
                                 })
                               }}/>
                </div>
                <Table className={classes.table}>
                
                    <TableHead>
                        <TableRow>
                            {
                                this.daysOfWeek.map((day, idx) => (
                                    <TableCell align="right" key={`courseDayKey-${idx}`}>{day}</TableCell>
                                ))
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            {
                                this.state.ready ? 
                                    this.renderTable().map((row, idx) => (
                                        <TableCell align="right" key={`courseDateKey-${idx}`}>
                                            <DayDate day={row.day}
                                                     start={row.start}
                                                     duration={row.duration}
                                                     status={row.status}
                                                     handleChange={this.handleChange}/>
                                        </TableCell>
                                    )) : null
                            }
                        </TableRow>
                    </TableBody>
                </Table>
                <Button onClick={this.handleUpdate}>Actualizar</Button>
            </Paper>
        )
    }

}

export default withStyles(styles)(CourseDate);