import React from 'react';
import { Admin, Resource } from 'react-admin';
import loopbackClient, { authProvider } from 'react-admin-loopback';
import { List, Datagrid, TextField, NumberField } from 'react-admin';

import { InstructorList, InstructorCreate, InstructorEdit } from './models/Instructor';
import { CourseList, CourseCreate, CourseEdit } from './models/Course';
import { TariffList, TariffCreate, TariffEdit } from './models/Tariff';
import { CourseDateList, CourseDateCreate, CourseDateEdit } from './models/CourseDate';
import { CustomerList, CustomerCreate, CustomerEdit } from './models/Customer';
import { CustomerCourseList, CustomerCourseCreate, CustomerCourseEdit } from './models/CustomerCourse';
import { CustomerCourseDateList, CustomerCourseDateCreate, CustomerCourseDateEdit } from './models/CustomerCourseDate';

import spanishMessages from 'aor-language-spanish';

const i18nProvider = locale => spanishMessages;

const App = () => (
  <Admin 
    dataProvider={loopbackClient('http://157.230.58.139:3000/api')} 
    authProvider={authProvider('http://157.230.58.139:3000/api/Admins/login')}
    i18nProvider={i18nProvider}>
    
    <Resource 
      name="Courses" 
      options={{
        label: 'Clases'
      }}
      list={CourseList} 
      create={CourseCreate}
      edit={CourseEdit}/>
    <Resource 
      name="Instructors"
      options={{
        label: 'Profesores'
      }}
      list={InstructorList} 
      create={InstructorCreate} 
      edit={InstructorEdit}/>
    <Resource 
      name="Customers"
      options={{
        label: 'Socios'
      }} 
      list={CustomerList} 
      create={CustomerCreate}
      edit={CustomerEdit}/>
    <Resource 
      name="Tariffs"
      options={{
        label: 'Tarifas'
      }} 
      list={TariffList} 
      create={TariffCreate}
      edit={TariffEdit}/>
    {/* <Resource 
      name="CourseDates" 
      list={CourseDateList} 
      create={CourseDateCreate}
      edit={CourseDateEdit}/> */}
    
    <Resource 
      name="CustomerCourses"
      options={{
        label: 'Clase de socios'
      }} 
      list={CustomerCourseList} 
      create={CustomerCourseCreate}
      edit={CustomerCourseEdit}/>
    {/* <Resource 
      name="CustomerCourseDates" 
      list={CustomerCourseDateList} 
      create={CustomerCourseDateCreate}
      edit={CustomerCourseDateEdit}/> */}

  </Admin>
);

export default App;
