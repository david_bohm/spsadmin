import React from 'react';

import {
    List,
    Datagrid,
    TextField,
    NumberField,
    ReferenceField,
    BooleanField
} from 'react-admin';

import {
    ShowButton,
    EditButton,
    Edit,
    SimpleForm,
    DisabledInput,
    DateInput,  
    TextInput,
    NumberInput,
    DateField,
    SelectInput,
    ReferenceInput,
    BooleanInput
} from 'react-admin';
import {
    Create,
    Toolbar,
    SaveButton
} from 'react-admin';
import {
    Show,
    SimpleShowLayout
} from 'react-admin';

const GenericEditToolbar = props => (
    <Toolbar {...props} >
        <SaveButton />
    </Toolbar>
);

export const CourseDateList = (props) => (
    <List {...props}>
        <Datagrid>
            <ReferenceField label="Course" source="id_course" reference="Courses">
                <TextField source="name" />
            </ReferenceField>
            <TextField source="day" />
            <TextField source="time_from" />
            <TextField source="hours" />
            <BooleanField source="status" />
            <EditButton basePath="/CourseDates" />
        </Datagrid>
    </List>
);

export const CourseDateCreate = (props) => (
    <Create title="Crear fecha de curso" {...props}>
        <SimpleForm defaultValue={{status: true}}>
            <ReferenceInput label="Course" source="id_course" reference="Courses">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <TextInput source="time_from" />
            <TextInput source="hours" />
            <SelectInput source="day" choices={[
                {id: 'Lunes', name: 'Lunes'},
                {id: 'Martes', name: 'Martes'},
                {id: 'Miercoles', name: 'Miércoles'},
                {id: 'Jueves', name: 'Jueves'},
                {id: 'Viernes', name: 'Viernes'},
                {id: 'Sabado', name: 'Sábado'},
                {id: 'Domingo', name: 'Domingo'}
            ]} />
            <BooleanInput source="status" />
        </SimpleForm>
    </Create>
);

export const CourseDateEdit = (props) => (
    <Edit title="Editar fecha de curso" {...props}>
        <SimpleForm  toolbar={<GenericEditToolbar/>}>
            <ReferenceInput label="Course" source="id_course" reference="Courses">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <TextInput source="time_from" />
            <TextInput source="hours" />
            <SelectInput source="day" choices={[
                {id: 'Lunes', name: 'Lunes'},
                {id: 'Martes', name: 'Martes'},
                {id: 'Miercoles', name: 'Miércoles'},
                {id: 'Jueves', name: 'Jueves'},
                {id: 'Viernes', name: 'Viernes'},
                {id: 'Sabado', name: 'Sábado'},
                {id: 'Domingo', name: 'Domingo'}
            ]} />
            <BooleanInput source="status" />
        </SimpleForm>
    </Edit>
);