import React from 'react';

import {
    List,
    Datagrid,
    TextField,
    NumberField,
    ReferenceField,
    BooleanField,
    TabbedForm,
    FormTab
} from 'react-admin';

import {
    ShowButton,
    EditButton,
    Edit,
    SimpleForm,
    DisabledInput,
    DateInput,  
    TextInput,
    NumberInput,
    DateField,
    SelectInput,
    ReferenceInput,
    BooleanInput,
    AutocompleteInput
} from 'react-admin';
import {
    Create,
    Toolbar,
    SaveButton
} from 'react-admin';
import {
    Show,
    SimpleShowLayout
} from 'react-admin';

import CourseDateCustomer from '../components/CourseDateCustomer';

const GenericEditToolbar = props => (
    <Toolbar {...props} >
        <SaveButton />
    </Toolbar>
);

const optionRenderer = choice => `${choice.name} ${choice.surname}`;

export const CustomerCourseList = (props) => (
    <List {...props} title="Lista de clases de socios">
        <Datagrid>
            <ReferenceField label="Alumno" source="id_customer" reference="Customers">
                <TextField source="name" />
            </ReferenceField>
            <ReferenceField label="Curso" source="id_course" reference="Courses">
                <TextField source="name" />
            </ReferenceField>
            <ReferenceField label="Tarifa" source="id_tariff" reference="Tariffs">
                <TextField source="name" />
            </ReferenceField>
            <BooleanField source="status" />
            <EditButton basePath="/CustomerCourses" />
        </Datagrid>
    </List>
);

export const CustomerCourseCreate = (props) => (
    <Create title="Crear fecha de curso" {...props}>
        <SimpleForm defaultValue={{status: true}}>
            <ReferenceInput label="Alumno" source="id_customer" reference="Customers">
                <AutocompleteInput optionText={optionRenderer} />
            </ReferenceInput>
            <ReferenceInput label="Curso" source="id_course" reference="Courses">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <ReferenceInput label="Tarifa" source="id_tariff" reference="Tariffs">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <BooleanInput source="status" />
        </SimpleForm>
    </Create>
);

export const CustomerCourseEdit = (props) => {
    console.log(props);
    return(
    <Edit title="Editar fecha de curso" {...props} >
        <TabbedForm toolbar={<GenericEditToolbar/>}>
            <FormTab label="General">
                <ReferenceInput label="Alumno" source="id_customer" reference="Customers">
                     <AutocompleteInput optionText={optionRenderer} />
                </ReferenceInput>
                <ReferenceInput label="Curso" source="id_course" reference="Courses">
                    <SelectInput optionText="name" />
                </ReferenceInput>
                <ReferenceInput label="Tarifa" source="id_tariff" reference="Tariffs">
                    <SelectInput optionText="name" />
                </ReferenceInput>
                <BooleanInput source="status" />

            </FormTab>
            <FormTab label="Días">
            <CourseDateCustomer courseId={props.id_course}/>
            </FormTab>
        </TabbedForm>
    </Edit>
)}