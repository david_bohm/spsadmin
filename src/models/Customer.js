import React from 'react';

import {
    List,
    Datagrid,
    TextField,
    NumberField,
    Filter
} from 'react-admin';

import {
    ShowButton,
    EditButton,
    Edit,
    SimpleForm,
    DisabledInput,
    DateInput,  
    TextInput,
    NumberInput,
    DateField,
    BooleanInput
} from 'react-admin';
import {
    Create,
    Toolbar,
    SaveButton
} from 'react-admin';
import {
    Show,
    SimpleShowLayout
} from 'react-admin';

import RichTextInput from 'ra-input-rich-text';

import CustomTextField from '../components/CustomTextField';

const GenericEditToolbar = props => (
    <Toolbar {...props} >
        <SaveButton />
    </Toolbar>
);

const CustomerFilter = (props) => 
    <Filter {...props}>
        {/* <TextInput label="Search" source="q" alwaysOn /> */}
        <BooleanInput label="Status" source="status"  />
        <TextInput label="DNI" source="identification"  />
        <TextInput label="Nombre" source="name"   />
        <TextInput label="Apellido" source="surname"   />
        <TextInput label="Obra social" source="healthinsurance"   />
    </Filter>

export const CustomerList = (props) => (
    <List {...props} title="Lista de socios" filters={<CustomerFilter/>}>
        <Datagrid>
            <TextField source="name" />
            <TextField source="surname" />
            <TextField source="identification_type" />
            <TextField source="identification" />
            <TextField source="address" />
            <TextField source="healthinsurance" />
            <TextField source="cellphone" />
            <TextField source="telephone" />
            <CustomTextField source="birthdate" />
            <EditButton basePath="/Customers" />
        </Datagrid>
    </List>
);

export const CustomerCreate = (props) => (
    <Create title="Crear socio" {...props}>
        <SimpleForm defaultValue={{status: true}}>
            <TextInput source="name" />
            <TextInput source="surname" />
            <TextInput source="identification_type" />
            <TextInput source="identification" />
            <TextInput source="address" />
            <TextInput source="healthinsurance" />
            <TextInput source="cellphone" />
            <TextInput source="telephone" />
            <DateInput source="birthdate" />
            <BooleanInput source="gender" />
            <BooleanInput source="status" />
            <RichTextInput source="notes" />
        </SimpleForm>
    </Create>
);

export const CustomerEdit = (props) => (
    <Edit title="Editar socio" {...props}>
        <SimpleForm  toolbar={<GenericEditToolbar/>}>
            <TextInput source="name" />
            <TextInput source="surname" />
            <TextInput source="identification_type" />
            <TextInput source="identification" />
            <TextInput source="address" />
            <TextInput source="healthinsurance" />
            <TextInput source="cellphone" />
            <TextInput source="telephone" />
            <TextInput source="birthdate" format={v => v.substring(0, 10)} />
            <BooleanInput source="gender" />
            <BooleanInput source="status" />
            <RichTextInput source="notes" />
        </SimpleForm>
    </Edit>
);