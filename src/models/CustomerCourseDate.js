import React from 'react';

import {
    List,
    Datagrid,
    TextField,
    NumberField,
    ReferenceField,
    BooleanField,
    TabbedForm,
    FormTab
} from 'react-admin';

import {
    ShowButton,
    EditButton,
    Edit,
    SimpleForm,
    DisabledInput,
    DateInput,  
    TextInput,
    NumberInput,
    DateField,
    SelectInput,
    ReferenceInput,
    BooleanInput
} from 'react-admin';
import {
    Create
} from 'react-admin';
import {
    Show,
    SimpleShowLayout
} from 'react-admin';

export const CustomerCourseDateList = (props) => (
    <List {...props}>
        <Datagrid>
            <ReferenceField label="Curso" source="id_customercourse" reference="CustomerCourses">
                <TextField source="id" />
            </ReferenceField>
            <ReferenceField label="Fecha de curso" source="id_coursedate" reference="CourseDates">
                <TextField source="day" />
            </ReferenceField>
            <BooleanField source="status" />
            <DateField source="createdAt" />
            <DateField source="updatedAt" />
            <EditButton basePath="/CustomerCourseDates" />
        </Datagrid>
    </List>
);

export const CustomerCourseDateCreate = (props) => (
    <Create title="Crear fecha de curso de socio" {...props}>
        <SimpleForm>
            <ReferenceInput label="Curso" source="id_customercourse" reference="CustomerCourses">
                <SelectInput optionText="id" />
            </ReferenceInput>
            <ReferenceInput label="Fecha de curso" source="id_coursedate" reference="CourseDates">
                <SelectInput optionText="day" />
            </ReferenceInput>
            <BooleanInput source="status" />
        </SimpleForm>
    </Create>
);

export const CustomerCourseDateEdit = (props) => (
    <Edit title="Editar fecha de curso de socio" {...props}>
        <TabbedForm>
            <FormTab label="General">
                <ReferenceInput label="Curso" source="id_customercourse" reference="CustomerCourses">
                    <SelectInput optionText="id" />
                </ReferenceInput>
            </FormTab>
            
            <BooleanInput source="status" />

        </TabbedForm>
    </Edit>
);