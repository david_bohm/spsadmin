import React from 'react';

import {
    List,
    Datagrid,
    TextField,
    NumberField,
    DateField,
    BooleanField,
    RichTextField,
    Filter
} from 'react-admin';

import {
    ShowButton,
    EditButton,
    Edit,
    SimpleForm,
    DisabledInput,
    DateInput,  
    TextInput,
    NumberInput,
    BooleanInput
} from 'react-admin';
import {
    Create,
    SaveButton,
    Toolbar
} from 'react-admin';
import {
    Show,
    SimpleShowLayout
} from 'react-admin';

import { ReferenceInput, SelectInput } from 'react-admin'

import RichTextInput from 'ra-input-rich-text';

const GenericEditToolbar = props => (
    <Toolbar {...props} >
        <SaveButton />
    </Toolbar>
);

const TariffFilter = (props) => 
    <Filter {...props}>
        {/* <TextInput label="Search" source="q" alwaysOn /> */}
        <BooleanInput label="Status" source="status"  />
        <TextInput label="Nombre" source="name"   />
    </Filter>

export const TariffList = (props) => (
    <List {...props} title="Lista de tarifas" >
        <Datagrid>
            <TextField source="name" />
            <NumberField source="amount"/>
            <BooleanField source="status" />
            <EditButton basePath="/Tariffs" />
        </Datagrid>
    </List>
);

export const TariffCreate = (props) => (
    <Create title="Crear tarifa" {...props}>
        <SimpleForm defaultValue={{status: true}}>
            <TextInput source="name" />
            <NumberInput source="amount" step={100}/>
            <BooleanInput source="status" />
        </SimpleForm>
    </Create>
);

export const TariffEdit = (props) => (
    <Edit title="Editar tarifas"  {...props}>
        <SimpleForm toolbar={<GenericEditToolbar/>} >
            <TextInput source="name" />
            <NumberInput source="amount" step={100}/>
            <BooleanInput source="status" />
        </SimpleForm>
    </Edit>
);

