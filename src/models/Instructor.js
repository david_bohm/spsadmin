import React from 'react';

import {
    List,
    Datagrid,
    TextField,
    NumberField,
    Filter
} from 'react-admin';

import {
    ShowButton,
    EditButton,
    Edit,
    SimpleForm,
    DisabledInput,
    DateInput,  
    TextInput,
    NumberInput,
    DateField,
    BooleanInput,
    Toolbar,
    SaveButton
} from 'react-admin';
import {
    Create
} from 'react-admin';
import {
    Show,
    SimpleShowLayout
} from 'react-admin';

const GenericEditToolbar = props => (
    <Toolbar {...props} >
        <SaveButton />
    </Toolbar>
);

const InstructorFilter = (props) => 
    <Filter {...props}>
        {/* <TextInput label="Search" source="q" alwaysOn /> */}
        <BooleanInput label="Status" source="status" />
        <TextInput label="DNI" source="identification"  />
        <TextInput label="Nombre" source="name"   />
        <TextInput label="Apellido" source="surname"  />
        <TextInput label="Obra social" source="healthinsurance" />
    </Filter>

export const InstructorList = (props) => (
    <List {...props} title="Lista de profesores" filters={<InstructorFilter/>} >
        <Datagrid>
            <TextField source="name" />
            <TextField source="surname" />
            <TextField source="identification_type" />
            <TextField source="identification" />
            <TextField source="address" />
            <TextField source="healthinsurance" />
            <TextField source="cellphone" />
            <TextField source="telephone" />
            <DateField source="birthdate" />
            <EditButton basePath="/Instructors" />
        </Datagrid>
    </List>
);

export const InstructorCreate = (props) => (
    <Create title="Crear profesor" {...props}>
        <SimpleForm defaultValue={{status: true}}>
            <TextInput source="name" />
            <TextInput source="surname" />
            <TextInput source="identification_type" />
            <TextInput source="identification" />
            <TextInput source="address" />
            <TextInput source="healthinsurance" />
            <TextInput source="cellphone" />
            <TextInput source="telephone" />
            <DateInput source="birthdate" />
            <BooleanInput source="gender" />
            <BooleanInput source="status" />
        </SimpleForm>
    </Create>
);

export const InstructorEdit = (props) => (
    <Edit title="Crear profesor" {...props} >
        <SimpleForm toolbar={<GenericEditToolbar/>}>
            <TextInput source="name" />
            <TextInput source="surname" />
            <TextInput source="identification_type" />
            <TextInput source="identification" />
            <TextInput source="address" />
            <TextInput source="healthinsurance" />
            <TextInput source="cellphone" />
            <TextInput source="telephone" />
            <TextInput source="birthday" />
            <BooleanInput source="gender" />
            <BooleanInput source="status" />
        </SimpleForm>
    </Edit>
);