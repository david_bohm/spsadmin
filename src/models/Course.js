import React from 'react';

import {
    List,
    Datagrid,
    TextField,
    NumberField,
    DateField,
    BooleanField,
    RichTextField,
    ReferenceField,
    Filter,
    Toolbar,
    SaveButton,
    TabbedForm,
    FormTab
} from 'react-admin';

import {
    ShowButton,
    EditButton,
    Edit,
    SimpleForm,
    DisabledInput,
    DateInput,  
    TextInput,
    NumberInput,
    BooleanInput,
    AutocompleteArrayInput,
    AutocompleteInput
} from 'react-admin';
import {
    Create
} from 'react-admin';
import {
    Show,
    SimpleShowLayout
} from 'react-admin';

import { ReferenceArrayInput, SelectArrayInput } from 'react-admin'

import { ReferenceInput, SelectInput } from 'react-admin';

import RichTextInput from 'ra-input-rich-text';

import CourseDate from '../components/CourseDate';
import CourseCustomers from '../components/CourseCustomers';

const GenericEditToolbar = props => (
    <Toolbar {...props} >
        <SaveButton />
    </Toolbar>
);

const CourseFilter = (props) => 
    <Filter {...props}>
        {/* <TextInput label="Search" source="q" alwaysOn /> */}
        <BooleanInput label="Status" source="status"  />
        <TextInput label="Nombre" source="name"   />
    </Filter>

export const CourseList = (props) => (
    <List {...props} title="Lista de clases" filters={<CourseFilter/>}>
        <Datagrid>
            <TextField source="name" />
            <TextField source="location" />
            <ReferenceField label="Instructor" source="id_instructor" reference="Instructors">
                <TextField source="name" />
            </ReferenceField>
            <BooleanField source="status" />
            <EditButton basePath="/Courses" />
        </Datagrid>
    </List>
);

export const CourseCreate = (props) => (
    <Create title="Crear curso" {...props}>
        <SimpleForm defaultValue={{status: true}}>
            <TextInput source="name" />
            <TextInput source="location" />
            <ReferenceInput label="Instructor" source="id_instructor" reference="Instructors">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <BooleanInput source="status"/>
            <RichTextInput source="notes" />
        </SimpleForm>
    </Create>
);

export const CourseEdit = (props) => {
    console.log(props);

    return (
    <Edit title="Editar curso" {...props} >
        <TabbedForm toolbar={<GenericEditToolbar/>}>
                <FormTab label="General">
                    <TextInput source="name" />
                    <TextInput source="location" />
                    <ReferenceInput label="Instructor" source="id_instructor" reference="Instructors">
                        <SelectInput optionText="name" />
                    </ReferenceInput>
                    <BooleanInput source="status" />
                    <RichTextInput source="notes" />

                </FormTab>
                <FormTab label="Horarios">
                    <CourseDate courseId={props.id} />

                </FormTab>
                <FormTab label="Alumnos">
                    <CourseCustomers />
                </FormTab>
            {/* <ReferenceArrayInput source="id_costumer" reference="Customers" label="Socios">
                <AutocompleteArrayInput source="name"/>
            </ReferenceArrayInput> */}
        </TabbedForm>
    </Edit>
)};

